const REFRESH_TOKEN = 'refresh-token'
const AUTH_TOKEN = 'access-token'
const USER_NAME = 'user-name'

export const getAccessToken = (): string => localStorage.getItem(AUTH_TOKEN)
export const getRefreshToken = (): string => localStorage.getItem(REFRESH_TOKEN)
export const getUserName = (): string => localStorage.getItem(USER_NAME)

export const isAuthenticated = (): boolean => getAccessToken() !== null

export const getAuth = (): Record<string, string> => ({
  accessToken: getAccessToken(),
  refreshToken: getRefreshToken()
})

export const setUserName = (username: string): void =>
  localStorage.setItem(USER_NAME, username)

export const setAccessToken = ({
  accessToken,
  refreshToken
}: {
  accessToken: string
  refreshToken: string
}): void => {
  localStorage.setItem(AUTH_TOKEN, accessToken)
  localStorage.setItem(REFRESH_TOKEN, refreshToken)
}

export const removeClaims = (): void => {
  localStorage.clear()
}
