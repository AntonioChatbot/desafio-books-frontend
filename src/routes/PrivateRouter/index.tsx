import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import Loading from '../../components/core/Loading'
import { isAuthenticated } from '../../lib/context/auth'

const PrivateRoute: React.FC = ({ children }: { children: React.FC }) => {
  const router = useRouter()
  const [auth, setAuth] = useState(false)

  useEffect(() => {
    if (isAuthenticated()) {
      setAuth(true)
    } else {
      router.push('/')
    }
  }, [])

  return (
    <>
      {auth && children}
      {!auth && <Loading />}
    </>
  )
}

export default PrivateRoute
