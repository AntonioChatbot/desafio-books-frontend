import { Method } from 'axios'
import { useState } from 'react'
import api from '../services/api'

type loadProps = {
  method: Method
  url: string
  body?: Record<string, string | number>
  headers?: Record<string, string>
}

type requestProps = {
  loading: boolean
  error: Record<string, any> // don't know what's the deep
  loadRequest: (data: loadProps) => Record<string, any> // don't know what's the deep
}

const useRequest = (): requestProps => {
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(null)

  const loadRequest = async ({ method, url, body, headers }: loadProps) => {
    setError(null)
    setLoading(true)
    try {
      const response = await api.request({
        method,
        url,
        data: body || null,
        headers: { ...api.defaults.headers, ...headers }
      })
      setLoading(false)
      return { response, status: response.status }
    } catch (err) {
      setError(err.response)
      setLoading(false)
      console.log({ err: err.response })
      return { response: err.response, status: err.response.status }
    }
  }

  return {
    loading,
    error,
    loadRequest
  }
}

export default useRequest
