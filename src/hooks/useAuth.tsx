import Router from 'next/router'
import { removeClaims } from '../lib/context/auth'
import api from '../services/api'
import { loginAuth, refreshTokenService } from '../services/login'

export const useAuth = async (status: number): Promise<void> => {
  if (status === 400) {
    removeClaims()
    Router.push('/')
  } else if (status === 401) {
    const { method, url, body, headers } = refreshTokenService()
    try {
      const response = await api.request({
        method,
        url,
        data: body || null,
        headers: { ...api.defaults.headers, ...headers }
      })
      loginAuth(
        response.headers.authorization,
        response.headers['refresh-token']
      )
    } catch (err) {
      removeClaims()
      Router.push('/')
    }
  }
}
