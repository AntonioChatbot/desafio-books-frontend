import Head from 'next/head'
import Image from 'next/image'
import { useRouter } from 'next/router'
import { useEffect } from 'react'
import LoginForm from '../components/form/Login'
import { isAuthenticated } from '../lib/context/auth'
import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  background-image: url('/images/background_login.png');
  min-height: 100vh;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  justify-content: center;
  flex-direction: column;
  align-items: self-start;

  @media (max-width: 576px) {
    background-position: 45% 0px;
  }
`

export const WrapTitle = styled.div`
  display: flex;
  padding-left: 115px;
  color: ${props => props.theme.colors.white};
  align-items: center;
  margin-bottom: 50px;

  span {
    font-weight: 300;
    font-size: 28px;
    margin-left: 16px;
  }

  @media (max-width: 576px) {
    padding-left: 0px;
    align-self: center;
    transform: translate(-45px, 8px);
  }
`

const Home: React.FC = () => {
  const router = useRouter()

  useEffect(() => {
    if (isAuthenticated()) {
      router.push('/books')
    }
  }, [])

  return (
    <div>
      <Head>
        <title>ioasys books</title>
      </Head>
      <main>
        <Container>
          <WrapTitle>
            <Image
              src="/images/logo.png"
              alt="Logo"
              width={104.4}
              height={36}
            />
            <span>Books</span>
          </WrapTitle>
          <LoginForm />
        </Container>
      </main>
    </div>
  )
}

export default Home
