import BookList from '../../components/BookList'
import Menu from '../../components/Menu'
import PrivateRoute from '../../routes/PrivateRouter'
import styled from 'styled-components'

const Container = styled.div`
  background-image: url('/images/background_books.png');
  padding: 42px 115px 16px;
  min-height: 100vh;
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;

  @media (max-width: 768px) {
    padding: 42px 20px 16px;
  }
`

const books: React.FC = () => {
  return (
    <PrivateRoute>
      <Container>
        <Menu />
        <BookList />
      </Container>
    </PrivateRoute>
  )
}

export default books
