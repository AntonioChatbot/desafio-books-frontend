import Image from 'next/image'
import { useRouter } from 'next/router'
import { getUserName, removeClaims } from '../../lib/context/auth'

import { Container, Logo, Profile } from './styles'
const menu: React.FC = () => {
  const router = useRouter()

  const logout = () => {
    removeClaims()
    router.push('/')
  }

  return (
    <Container>
      <Logo>
        <Image src="/images/logo.png" alt="logo" width={104.4} height={36} />
        <span>Books</span>
      </Logo>
      <Profile>
        <p>
          Bem vindo<strong>, {getUserName()}!</strong>
        </p>
        <a onClick={logout}>
          <Image src="/icons/logout.png" alt="logout" width={32} height={32} />
        </a>
      </Profile>
    </Container>
  )
}

export default menu
