import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  justify-content: space-between;
`

export const Logo = styled.div`
  display: flex;
  align-items: center;

  img {
    filter: invert(1);
  }

  span {
    margin-left: 16.6px;
    font-weight: 300;
    font-size: 28px;
  }
`

export const Profile = styled.div`
  display: flex;
  align-items: center;

  p {
    margin-right: 16px;
    font-size: 12px;
    line-height: 16px;
    font-weight: normal;

    strong {
      font-size: 12px;
      font-weight: 600;
    }
  }

  a {
    display: flex;
    cursor: pointer;
  }

  @media (max-width: 576px) {
    p {
      display: none;
    }
  }
`
