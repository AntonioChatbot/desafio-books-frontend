import { Story } from '@storybook/react'

import Menu from './index'

export default {
  title: 'Components/Menu',
  component: Menu
}

const Template: Story = args => <Menu {...args} />

export const Simple = Template.bind({})
Simple.args = {}
