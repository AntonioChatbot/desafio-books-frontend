import { Story } from '@storybook/react'
import { ThemeProvider } from 'styled-components'
import theme from '../../styles/theme'

import BookList from './index'

export default {
  title: 'Components/BookList',
  component: BookList
}

const Template: Story = args => (
  <ThemeProvider theme={theme}>
    <BookList {...args} />
  </ThemeProvider>
)

export const Simple = Template.bind({})
Simple.args = {}
