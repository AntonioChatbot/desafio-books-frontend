import Card from '../Card'
import Pagination from '../Pagination'
import BookModal from '../BookModal'

import { Grid } from './styles'
import { useCallback, useEffect, useState } from 'react'
import useRequest from '../../hooks/useRequest'
import { booksService } from '../../services/Book'
import BookInterface from '../../constants/interfaces/bookInterface'
import { useDebounce } from '../../hooks/useDebounce'
import { useAuth } from '../../hooks/useAuth'

const bookList: React.FC = () => {
  const [showModal, setShowModal] = useState('none')
  const [books, setBooks] = useState(null)
  const [bookId, setBookId] = useState(null)
  const [page, setPage] = useState(1)
  const [totalPages, setTotalPages] = useState(null)
  const { loadRequest, loading } = useRequest()
  const paginationFilter = useDebounce(page, 500)

  const amount = 12

  useEffect(() => {
    getBooks()
  }, [paginationFilter])

  const getBooks = useCallback(async () => {
    const { response, status } = await loadRequest(booksService(page, amount))
    useAuth(status)
    setTotalPages(Math.ceil(response.data.totalPages))
    setBooks(response.data.data)
  }, [paginationFilter])

  const setModal = (param: boolean, id?: string) => {
    if (param) {
      setShowModal('flex')
      setBookId(id || null)
    } else {
      setShowModal('none')
    }
    hideScrollbar(param)
  }

  const hideScrollbar = (param: boolean) => {
    const body = document.getElementsByTagName('body')[0]
    if (param) {
      body.style.overflowY = 'hidden'
    } else {
      body.style.overflowY = 'auto'
    }
  }

  return (
    <>
      {bookId && (
        <BookModal showModal={showModal} setModal={setModal} bookId={bookId} />
      )}
      <Grid>
        {books &&
          books.map((book: BookInterface, index: number) => (
            <Card
              key={index}
              loading={loading}
              setModal={setModal}
              book={book}
            />
          ))}
      </Grid>
      <Pagination
        currentPage={page}
        totalPages={totalPages}
        setPage={setPage}
      />
    </>
  )
}

export default bookList
