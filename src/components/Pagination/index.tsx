import { useEffect, useState } from 'react'
import Arrow from '../../assets/icons/Arrow'

import { Container } from './styles'

type PaginationProps = {
  currentPage: number
  totalPages: number
  setPage: (number) => void
}

const Pagination: React.FC<PaginationProps> = ({
  currentPage,
  totalPages,
  setPage
}) => {
  const disabled = '#777676'
  const enabled = '#000'

  const [backDisabled, setBackDisabled] = useState(true)
  const [nextDisabled, setNextDisabled] = useState(false)

  useEffect(() => {
    checkDisabled()
  }, [currentPage])

  const checkDisabled = () => {
    if (currentPage === totalPages) {
      setNextDisabled(true)
      return
    }
    if (currentPage <= 1) {
      setBackDisabled(true)
      return
    }
    setNextDisabled(false)
    setBackDisabled(false)
  }

  const nextPage = () => {
    setPage(currentPage + 1)
  }

  const backPage = () => {
    setPage(currentPage - 1)
  }

  return (
    <Container>
      <p>
        Página {currentPage} de {totalPages}
      </p>
      <button
        onClick={backPage}
        disabled={backDisabled}
        style={{ marginRight: '8px', transform: 'rotate(180deg)' }}
      >
        <Arrow fill={backDisabled ? disabled : enabled} />
      </button>
      <button disabled={nextDisabled} onClick={nextPage}>
        <Arrow fill={nextDisabled ? disabled : enabled} />
      </button>
    </Container>
  )
}

export default Pagination
