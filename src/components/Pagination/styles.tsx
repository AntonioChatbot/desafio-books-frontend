import styled from 'styled-components'

export const Container = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: flex-end;
  align-items: center;
  margin-right: 8px;

  p {
    margin-right: 16px;
    font-size: 12px;
    line-height: 20px;
    font-weight: normal;
  }

  button {
    cursor: pointer;
    outline: none;
    display: flex;
    border: none;
    background: transparent;
  }
`
