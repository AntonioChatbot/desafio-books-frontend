import styled from 'styled-components'

export const InputContainer = styled.div`
  background: rgba(0, 0, 0, 0.32);
  backdrop-filter: blur(2px);
  border-radius: 4px;
  padding: 8px 16px;
  width: 368px;
  margin-bottom: 16px;

  @media (max-width: 576px) {
    width: 288px;
  }
`

export const Label = styled.p`
  color: ${props => props.theme.colors.white};
  font-size: 12px;
  font-style: normal;
  font-weight: normal;
  opacity: 0.5;
`

export const Input = styled.input`
  cursor: auto;
  border: none;
  width: 240px;
  background: transparent;
  outline: none;
  font-size: 16px;
  font-weight: 400;
  color: ${props => props.theme.colors.white};

  ::placeholder {
    color: rgba(255, 255, 255, 0.4);
  }

  @media (max-width: 576px) {
    width: 158px;
  }
`

type buttonProps = {
  display: string
}

export const Button = styled.button<buttonProps>`
  position: absolute;
  transform: translate(10px, -14px);
  display: ${({ display }) => (display === 'true' ? 'initial' : 'none')};
  width: 85px;
  height: 36px;
  font-size: 16px;
  background: #ffffff;
  border-radius: 44px;
  border: none;
  outline: none;
  font-style: normal;
  font-weight: 500;
  line-height: 20px;
  cursor: pointer;
  color: ${props => props.theme.colors.purple};
`

export const Error = styled.p`
  padding: 4px 16px;
  margin-top: -16px;
  color: #ff0000;
  font-size: 12px;
`
