import { InputContainer, Label, Input, Error, Button } from './styles'

type InputFieldProps = {
  error: Record<string, any>
  label: string
  errorMessage?: string
  testId?: string
  button?: boolean
  innerRef?: any
  placeholder: string
  type: string
  name: string
}

const InputField: React.FC<InputFieldProps> = ({
  error,
  label,
  errorMessage,
  button,
  innerRef,
  testId,
  placeholder,
  name,
  type
}) => {
  return (
    <>
      <InputContainer>
        <Label>{label}</Label>
        <Input
          data-testid={testId || 'input-field'}
          placeholder={placeholder}
          name={name}
          type={type}
          ref={innerRef}
        />
        <Button display={String(button)} type="submit">
          Entrar
        </Button>
      </InputContainer>
      {!error && errorMessage && <Error>{errorMessage}</Error>}
    </>
  )
}

export default InputField
