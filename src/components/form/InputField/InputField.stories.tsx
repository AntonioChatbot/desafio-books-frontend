import { Story } from '@storybook/react'
import { ThemeProvider } from 'styled-components'
import theme from '../../../styles/theme'
import InputField from './index'

export default {
  title: 'Components/InputField',
  component: InputField
}

type InputFieldProps = {
  error: Record<string, any>
  label: string
  errorMessage?: string
  id?: string
  button?: boolean
  innerRef?: any
  placeholder: string
  type: string
  name: string
}

const Template: Story<InputFieldProps> = args => (
  <ThemeProvider theme={theme}>
    <div style={{ background: '#B22E6F', height: '100vh' }}>
      <InputField {...args} />
    </div>
  </ThemeProvider>
)

export const WithoutButton = Template.bind({})
WithoutButton.args = {
  error: { teste: 'teste' },
  label: 'Senha',
  placeholder: 'digite sua senha',
  name: 'password',
  type: 'password'
}

export const WithButton = Template.bind({})
WithButton.args = {
  button: true,
  error: { teste: 'teste' },
  label: 'Senha',
  placeholder: 'digite sua senha',
  name: 'password',
  type: 'password'
}
