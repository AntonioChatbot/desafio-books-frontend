import { render, fireEvent, waitFor } from '@testing-library/react'
import { ThemeProvider } from 'styled-components'
import InputField from './index'
import theme from '../../../styles/theme'

describe('Text Field Tests', () => {
  it('Should Change Input', async () => {
    const { getByTestId } = render(
      <ThemeProvider theme={theme}>
        <InputField type="email" />
      </ThemeProvider>
    )

    const value = 'antoniohamilton.s.freitas@gmail.com'

    const inputField = await waitFor(() => getByTestId('input-field'))

    fireEvent.change(inputField, { target: { value } })

    expect(inputField.value).toEqual(value)
  })
})
