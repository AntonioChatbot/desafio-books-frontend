import useRequest from '../../../hooks/useRequest'
import { useForm } from 'react-hook-form'
import { Form, Error } from './styles'
import { loginService, loginAuth } from '../../../services/login'
import { useRouter } from 'next/router'
import { setUserName } from '../../../lib/context/auth'
import Loading from '../../core/Loading'
import { REGEX_EMAIL } from '../../../constants/regex'
import InputField from '../InputField'

const login: React.FC = () => {
  type Inputs = {
    email: string
    password: string
  }

  const { register, handleSubmit, errors } = useForm<Inputs>({
    mode: 'onChange'
  })
  const { loading, error, loadRequest } = useRequest()
  const router = useRouter()

  const onSubmit = async value => {
    const { response } = await loadRequest(loginService(value))
    if (response?.headers?.authorization) {
      loginAuth(
        response.headers.authorization,
        response.headers['refresh-token']
      )
      setUserName(response.data.name)
      router.push('/books')
    }
  }

  return (
    <Form onSubmit={handleSubmit(onSubmit)}>
      <InputField
        testId="email-field"
        error={error}
        innerRef={register({
          required: { value: true, message: 'Esse campo é obrigatório' },
          pattern: { value: REGEX_EMAIL, message: 'Insira um email válido' }
        })}
        errorMessage={errors?.email?.message}
        label="Email"
        placeholder="digite seu email"
        name="email"
        type="email"
      />
      <InputField
        testId="password-field"
        button={true}
        error={error}
        innerRef={register({
          required: { value: true, message: 'Esse campo é obrigatório' }
        })}
        errorMessage={errors?.password?.message}
        label="Senha"
        placeholder="digite sua senha"
        name="password"
        type="password"
      />
      {error && <Error>Email e/ou senha incorretos.</Error>}
      {loading && <Loading />}
    </Form>
  )
}
export default login
