import styled from 'styled-components'

export const Form = styled.form`
  padding-left: 115px;

  @media (max-width: 576px) {
    padding: 0;
    align-self: center;
  }
`

export const Error = styled.p`
  font-weight: 700;
  font-size: 16px;
  line-height: 16px;
  font-style: normal;
  color: ${props => props.theme.colors.white};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  background: rgba(255, 255, 255, 0.4);
  backdrop-filter: blur(2px);
  width: 239px;
  height: 48px;

  ::before {
    content: '';
    position: absolute;
    left: 17px;
    top: -10px;
    width: 0;
    height: 0;
    border-left: 10px solid transparent;
    border-right: 10px solid transparent;
    border-bottom: 10px solid rgba(255, 255, 255, 0.4);
  }
`
