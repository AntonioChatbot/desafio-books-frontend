import { Story } from '@storybook/react'
import { ThemeProvider } from 'styled-components'
import theme from '../../../styles/theme'

import LoginForm from './index'

export default {
  title: 'Components/LoginForm',
  component: LoginForm
}

const Template: Story = args => (
  <ThemeProvider theme={theme}>
    <div style={{ background: '#B22E6F', height: '100vh' }}>
      <LoginForm {...args} />
    </div>
  </ThemeProvider>
)

export const Simple = Template.bind({})
Simple.args = {}
