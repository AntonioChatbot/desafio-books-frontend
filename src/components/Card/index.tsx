import BookInterface from '../../constants/interfaces/bookInterface'
import {
  Container,
  Info,
  Title,
  Author,
  Details,
  Image,
  LoadingCard
} from './styles'

type FuncProps = {
  setModal(param: boolean, id: string): void
  book: BookInterface
  loading: boolean
}

const Card: React.FC<FuncProps> = ({ setModal, book, loading }) => {
  return (
    <>
      {!loading && (
        <Container onClick={() => setModal(true, book.id)}>
          <Image src={book.imageUrl || '/images/Book.png'} alt="book" />
          <Info>
            <Title>{book.title}</Title>
            {book.authors.map(author => (
              <Author key={author}>{author}</Author>
            ))}
            <Details>
              <p data-testid="pages">{book.pageCount} páginas</p>
              <p>Editora: {book.publisher}</p>
              <p>Publicado em {book.published}</p>
            </Details>
          </Info>
        </Container>
      )}
      {loading && <LoadingCard />}
    </>
  )
}

export default Card
