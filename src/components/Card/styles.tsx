import styled, { css, keyframes } from 'styled-components'

export const Container = styled.a`
  background: ${props => props.theme.colors.white};
  display: flex;
  padding: 19px 16px;
  border-radius: 4px;
  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);

  :hover {
    cursor: pointer;
    box-shadow: 0px 16px 80px rgba(84, 16, 95, 0.32);
  }
`

const Skeleton = keyframes`
  from {
    background: #999
  }
  to {
    background: #999999b0
  }
`

export const LoadingCard = styled.div`
  width: 100%;
  height: 180px;
  background: ${props => props.theme.colors.gray};
  border-radius: 4px;
  animation: 1s ${Skeleton} infinite alternate ease-in-out;
`

export const Image = styled.img`
  align-self: center;
  max-width: 81px;
`

export const Info = styled.article`
  display: flex;
  flex-direction: column;
  height: fit-content;
  margin-left: 16px;
`

export const Title = styled.h1`
  font-size: 14px;
  font-weight: 500;
  line-height: 20px;
`

export const Author = styled.h2`
  font-size: 12px;
  font-weight: normal;
  line-height: 20px;
  color: ${props => props.theme.colors.pink};
`

export const Details = styled.div`
  margin-top: 25px;

  p {
    color: ${props => props.theme.colors.gray};
    font-size: 12px;
    font-weight: normal;
    line-height: 20px;
  }
`
