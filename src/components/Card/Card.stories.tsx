import { Story } from '@storybook/react'
import { ThemeProvider } from 'styled-components'
import BookInterface from '../../constants/interfaces/bookInterface'
import theme from '../../styles/theme'

import Card from './index'

export default {
  title: 'Components/Card',
  component: Card
}

type FuncProps = {
  setModal(param: boolean, id: string): void
  book: BookInterface
  loading: boolean
}

const Template: Story<FuncProps> = args => (
  <ThemeProvider theme={theme}>
    <Card {...args} />
  </ThemeProvider>
)

export const Loaded = Template.bind({})
Loaded.args = {
  book: {
    authors: ['Fábio Souza', 'Sr. Danilo Costa', 'Núbia Reis'],
    title: 'Ad',
    description:
      'Culpa aut sapiente voluptatem dicta unde qui. Reiciendis nesciunt voluptatem suscipit dolores iusto non officia. Accusantium magnam quia omnis iste. Culpa autem inventore quia quasi magnam dolor. Magnam fugiat explicabo qui.\n \rIllo ut officiis culpa fuga inventore aut maxime possimus. Velit dicta nesciunt quis doloribus id sit. Vel est qui odio.',
    pageCount: 1821,
    category: 'Crítica Literária',
    imageUrl: 'https://files-books.ioasys.com.br/Book-10.jpg',
    language: 'Português',
    isbn10: '9043405607',
    isbn13: '978-9043405607',
    publisher: 'Braga S.A.',
    published: 2009,
    id: '60171639faf5de22b804a150'
  },
  loading: false,
  setModal: (param, id) => {
    console.log(param, id)
  }
}

export const Loading = Template.bind({})
Loading.args = {
  book: {
    authors: ['Fábio Souza', 'Sr. Danilo Costa', 'Núbia Reis'],
    title: 'Ad',
    description:
      'Culpa aut sapiente voluptatem dicta unde qui. Reiciendis nesciunt voluptatem suscipit dolores iusto non officia. Accusantium magnam quia omnis iste. Culpa autem inventore quia quasi magnam dolor. Magnam fugiat explicabo qui.\n \rIllo ut officiis culpa fuga inventore aut maxime possimus. Velit dicta nesciunt quis doloribus id sit. Vel est qui odio.',
    pageCount: 1821,
    category: 'Crítica Literária',
    imageUrl: 'https://files-books.ioasys.com.br/Book-10.jpg',
    language: 'Português',
    isbn10: '9043405607',
    isbn13: '978-9043405607',
    publisher: 'Braga S.A.',
    published: 2009,
    id: '60171639faf5de22b804a150'
  },
  loading: true,
  setModal: (param, id) => {
    console.log(param, id)
  }
}
