import { useEffect, useState } from 'react'
import { bookService } from '../../services/Book'
import Image from 'next/image'
import Loading from '../core/Loading'
import useRequest from '../../hooks/useRequest'

import {
  Container,
  Modal,
  CloseButton,
  WrapInfo,
  Title,
  Author,
  Details,
  Description,
  WrapData,
  WrapImage,
  Overlay,
  BookImage
} from './styles'
import { useAuth } from '../../hooks/useAuth'

type BookModalProps = {
  setModal(param: boolean): void
  showModal: string
  bookId: string
}

const BookModal: React.FC<BookModalProps> = ({
  setModal,
  showModal,
  bookId
}) => {
  const [book, setBook] = useState(null)
  const [authors, setAuthors] = useState(null)
  const { loadRequest, loading } = useRequest()

  useEffect(() => {
    getBook()
  }, [bookId])

  const getBook = async () => {
    const { response, status } = await loadRequest(bookService(bookId))
    useAuth(status)
    setBook(response.data)
    setAuthors(response.data.authors.join(', '))
  }

  return (
    <Container display={showModal}>
      <Overlay onClick={() => setModal(false)}></Overlay>
      <CloseButton onClick={() => setModal(false)}>
        <Image src="/icons/close.png" alt="close" width={32} height={32} />
      </CloseButton>
      <Modal>
        {loading && <Loading />}
        {!loading && book && (
          <>
            <BookImage src={book.imageUrl || '/images/Book.png'} alt="book" />
            <WrapInfo>
              <Title>{book.title}</Title>
              <Author>{authors}</Author>
              <Details>
                <h1>INFORMAÇÕES</h1>
                <Data title={'Páginas'} info={book.pageCount} />
                <Data title={'Editora'} info={book.publisher} />
                <Data title={'Publicação'} info={book.published} />
                <Data title={'Idioma'} info={book.language} />
                <Data title={'Título Original'} info={book.title} />
                <Data title={'ISBN-10'} info={book.isbn10} />
                <Data title={'ISBN-13'} info={book.isbn13} />
              </Details>
              <Description>
                <WrapImage>
                  <Image
                    src="/icons/quotes.png"
                    alt="book"
                    width={17}
                    height={15}
                  />
                </WrapImage>
                <span>{book.description}</span>
              </Description>
            </WrapInfo>
          </>
        )}
      </Modal>
    </Container>
  )
}

export default BookModal

type DataProps = {
  title: string
  info: string | number
}

const Data: React.FC<DataProps> = ({ title, info }) => {
  return (
    <WrapData>
      <h3>{title}</h3>
      <p>{info}</p>
    </WrapData>
  )
}
