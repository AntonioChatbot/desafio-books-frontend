import styled from 'styled-components'

type ContainerProps = {
  display: string
}

export const Container = styled.div<ContainerProps>`
  display: ${props => props.display};
  position: fixed;
  z-index: 2;
  height: 100vh;
  width: 100vw;
  top: 0;
  left: 0;
  align-items: center;
  justify-content: center;
`

export const Overlay = styled.div`
  height: 100vh;
  width: 100vw;
  position: absolute;
  z-index: 3;
  background: rgba(0, 0, 0, 0.3);
  top: 0;
  left: 0;
  backdrop-filter: blur(2px);
`

export const CloseButton = styled.button`
  display: flex;
  position: absolute;
  top: 16px;
  right: 17px;
  border: none;
  outline: none;
  cursor: pointer;
  background: transparent;
  z-index: 4;
`

export const Modal = styled.div`
  display: flex;
  background: ${props => props.theme.colors.white};
  width: 900px;
  height: 608px;
  box-shadow: 0px 16px 80px rgba(0, 0, 0, 0.32);
  border-radius: 4px;
  padding: 48px;
  z-index: 4;

  @media (max-width: 992px) {
    width: 100%;
    margin: 0px 20px;
  }

  @media (max-width: 768px) {
    overflow-y: scroll;
    overflow-x: hidden;
    flex-direction: column;
    padding: 24px;
    height: 85vh;
    position: relative;
    top: 24px;

    ::-webkit-scrollbar {
      width: 6px;
    }

    ::-webkit-scrollbar-track {
      border-radius: 3px;
      background: rgba(153, 153, 153, 0.507);
    }

    ::-webkit-scrollbar-thumb {
      border-radius: 4px;
      background: ${props => props.theme.colors.primary};
    }
  }
`

export const BookImage = styled.img`
  max-width: 350px;
  max-height: 513px;

  @media (max-width: 768px) {
    align-self: center;
    width: 240px;
    height: 351px;
    margin-bottom: 24px;
  }
`

export const WrapInfo = styled.div`
  width: 50%;
  margin-left: 48px;

  @media (max-width: 768px) {
    margin-left: 0px;
    width: 100%;
  }
`

export const Title = styled.h1`
  font-weight: 500;
  font-size: 28px;
  line-height: 40px;
  overflow: hidden;
  text-overflow: ellipsis;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`

export const Author = styled.p`
  margin-bottom: 32px;
  font-size: 12px;
  line-height: 16px;
  font-weight: normal;
  color: ${props => props.theme.colors.pink};
`

export const Details = styled.div`
  margin-bottom: 32px;

  h1 {
    font-size: 12px;
    font-weight: 500;
    margin-bottom: 14px;
  }
`

export const WrapData = styled.div`
  display: flex;
  justify-content: space-between;

  h3,
  p {
    font-size: 12px;
  }

  h3 {
    font-weight: 500;
  }

  p {
    font-weight: 400;
    color: ${props => props.theme.colors.gray};
  }
`

export const Description = styled.div`
  overflow-y: auto;
  height: 194px;

  ::-webkit-scrollbar {
    width: 5px;
  }

  ::-webkit-scrollbar-track {
    border-radius: 4px;
    background: rgba(153, 153, 153, 0.507);
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 4px;
    background: ${props => props.theme.colors.gray};
  }

  @media (max-width: 768px) {
    overflow-y: auto;
    height: auto;
  }
`

export const WrapImage = styled.span`
  margin-right: 5px;
`
