import Lottie from 'react-lottie'
import loading from '../../../assets/animations/loading.json'

import { Container } from './styles'

const Loading: React.FC = () => {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: loading,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  return (
    <Container>
      <Lottie
        options={defaultOptions}
        height={300}
        width={300}
        isStopped={false}
        isPaused={false}
        style={{ cursor: 'default' }}
        isClickToPauseDisabled={true}
      />
    </Container>
  )
}

export default Loading
