import { Story } from '@storybook/react'

import Loading from './index'

export default {
  title: 'Components/Loading',
  component: Loading
}

const Template: Story = args => <Loading {...args} />

export const Simple = Template.bind({})
Simple.args = {}
