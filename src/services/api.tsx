import axios from 'axios'

import { baseURL } from '../constants/endpoints'
import {
  getAccessToken,
  isAuthenticated,
  removeClaims
} from '../lib/context/auth'
import { refreshTokenService } from './login'

const api = axios.create({
  baseURL,
  timeout: 5000,
  headers: {
    'Content-Type': 'application/json'
  }
})

api.interceptors.request.use(async initialConfig => {
  const config = initialConfig
  if (isAuthenticated()) {
    config.headers.Authorization = `Bearer ${getAccessToken()}`
  }
  return config
})

export default api
