import { Method } from 'axios'

interface ServiceInterface {
  method: Method
  url: string
  headers?: Record<string, string>
  body?: Record<string, any>
}

export default ServiceInterface
