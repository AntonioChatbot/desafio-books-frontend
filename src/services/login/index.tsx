import {
  getAccessToken,
  getRefreshToken,
  setAccessToken
} from '../../lib/context/auth'
import ServiceInterface from '../serviceInterface'
const URILogin = '/auth/sign-in'
const URIRefresh = '/auth/refresh-token'

export const loginService = (
  body: Record<string, string | number>
): ServiceInterface => {
  return {
    method: 'post',
    url: URILogin,
    body
  }
}

export const refreshTokenService = (): ServiceInterface => {
  return {
    method: 'post',
    url: URIRefresh,
    body: { refreshToken: getRefreshToken() },
    headers: {
      Authorization: `Bearer ${getAccessToken}`
    }
  }
}

export const loginAuth = (accessToken: string, refreshToken: string): void => {
  setAccessToken({ accessToken, refreshToken })
}
