import ServiceInterface from '../serviceInterface'

const URI = '/books'

export const booksService = (
  page: number,
  amount: number
): ServiceInterface => {
  return {
    method: 'get',
    url: `${URI}?page=${page}&amount=${amount}`
  }
}

export const bookService = (id: string): ServiceInterface => {
  return {
    method: 'get',
    url: `${URI}/${id}`
  }
}
