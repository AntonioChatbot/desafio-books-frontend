const theme = {
  colors: {
    background: '#e1e1e6',
    text: '#121214',
    primary: '#8257e6',
    blue: '#5076F2',
    white: '#fff',
    purple: '#B22E6F',
    pink: '#AB2680',
    gray: '#999999'
  }
}

export default theme
