# ABOUT

The Project is a challenge provides by ioasys, the idea is to show books and informations about it

# Team

- [Antonio Hamilton Santos Freitas](https://www.linkedin.com/in/antonio-hamilton/) - `Front-end developer`

# Libraries

## Dev dependencies

- [@storybook/addon-actions: ^6.1.20](https://storybook.js.org)
- [@storybook/addon-essentials: ^6.1.20](https://storybook.js.org)
- [@storybook/addon-links: ^6.1.20](https://storybook.js.org)
- [@storybook/react: ^6.1.20](https://storybook.js.org)
- [@testing-library/react: ^11.2.5](https://testing-library.com)
- [@types/node: ^14.14.28](https://www.npmjs.com/package/@types/node)
- [@types/react: ^17.0.2](https://www.npmjs.com/package/@types/react)
- [@types/styled-components: ^5.1.7](https://styled-components.com)
- [@typescript-eslint/eslint-plugin: ^4.15.1](https://www.typescriptlang.org)
- [@typescript-eslint/parser: ^4.15.1](https://www.typescriptlang.org)
- [babel-loader: ^8.2.2](https://babeljs.io)
- [eslint: ^7.20.0](https://eslint.org)
- [eslint-config-prettier: ^7.2.0](https://eslint.org)
- [eslint-config-standard: ^16.0.2](https://eslint.org)
- [eslint-plugin-import: ^2.22.1](https://eslint.org)
- [eslint-plugin-node: ^11.1.0](https://eslint.org)
- [eslint-plugin-prettier: ^3.3.1](https://eslint.org)
- [eslint-plugin-promise: ^4.3.1](https://eslint.org)
- [eslint-plugin-react: ^7.22.0](https://eslint.org)
- [prettier: ^2.2.1](https://prettier.io)
- [typescript: ^4.1.5](https://www.typescriptlang.org)
- [jest](https://jestjs.io)
- [react-lottie](https://www.npmjs.com/package/react-lottie)
- [react-hook-form](https://react-hook-form.com)
- [sb](https://storybook.js.org)

# Dependencies

- [axios: ^0.21.1](github.com/axios/axios)
- [framer-motion: ^3.6.7](https://www.framer.com/motion/)
- [next: 10.0.7](https://nextjs.org)
- [react: 17.0.1](https://pt-br.reactjs.org)
- [react-dom: 17.0.1](https://pt-br.reactjs.org)

# How to Run

First, you need install dependencies, if you prefer use the yarn, you just need execute the command `yarn` otherwise you can execute `npm install` or `npm i`

## Tests

- Run on your terminal `yarn test` or `npm test` as you prefer

## Dev

- Run on your terminal `yarn dev` or `npm dev`

## Build

- Run on your terminal `yarn build` or `npm build`

# Deploy

- The project was deployed in Vercel, because is free and are analytics stats for next
- To have access ask [Antonio Hamilton Santos Freitas](https://www.linkedin.com/in/antonio-hamilton/)

# Stack

- React
- Next
